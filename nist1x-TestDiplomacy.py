from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_place, diplomacy_armys, army_fix, diplomacy_attackers, diplomacy_supporters, place_armys, diplomacy_print, diplomacy_afterMath, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_dplace_1(self):
        self.assertEqual(diplomacy_place("A Madrid Hold"),{"A":"Madrid"})
    def test_dplace_2(self):
        self.assertEqual(diplomacy_place("A"),"Error invalid input")

    def test_darmys_1(self):
        self.assertEqual(diplomacy_armys("A Madrid",{}),"Error invalid input")
    def test_darmys_2(self):
        self.assertEqual(diplomacy_armys("A Madrid Hold",{}),{"A":1})
    def test_darmys_3(self):
        self.assertEqual(diplomacy_armys("A Madrid Support",{}),"Error invalid input")
    def test_darmys_4(self):
        self.assertEqual(diplomacy_armys("A Madrid Move London",{}),{"A":1})
    def test_darmys_5(self):
        self.assertEqual(diplomacy_armys("A Madrid Support B",{}),{"A":1})
    def test_darmys_6(self):
        self.assertEqual(diplomacy_armys("A Madrid Support B",{"B":1}),{"A":1})

    def test_fix_1(self):
        self.assertEqual(army_fix("A Madrid",{}),"Error invalid input")
    def test_fix_2(self):
        self.assertEqual(army_fix("A Madrid Support",{}),"Error invalid input")
    def test_fix_3(self):
        self.assertEqual(army_fix("A Madrid Support B",{}),"Fixed")

    def test_dattackers_1(self):
        self.assertEqual(diplomacy_attackers("A Madrid Support B",{}),{})
    def test_dattackers_2(self):
        self.assertEqual(diplomacy_attackers("A Madrid Move London",{"A":"Madrid", "B":"London"}),{"A":"B"})
    def test_dattackers_3(self):
        self.assertEqual(diplomacy_attackers("A Madrid Move",{"A":"Madrid", "B":"London"}),"Error invalid input")

    def test_dsupporters_1(self):
        self.assertEqual(diplomacy_supporters("A Madrid Move B",{}),{})
    def test_dsupporters_2(self):
        self.assertEqual(diplomacy_supporters("A Madrid Support B",{"A":"Madrid","B":"London"}),{"A":"B"})
    def test_dsupporters_3(self):
        self.assertEqual(diplomacy_supporters("A Madrid Support",{"A":"Madrid", "B":"London"}),"Error invalid input")

    def test_parmys_1(self):
        self.assertEqual(dict(place_armys("A","Madrid")),{"A":"Madrid"})

    def test_dprint_1(self):
        w = StringIO()
        diplomacy_print(w, "1 10 20")
        self.assertEqual(w.getvalue(), "1 10 20\n")


    def test_dafterM_1(self):
        locations={"A":"Madrid","B":"London","C":"Paris"}
        armyValues ={"A":1,"B":2,"C":1}
        attackers = {"C":"B"}
        supporters = {"A":"B"}
        diplomacy_afterMath(locations,armyValues,attackers,supporters)
        self.assertEqual(locations,{"A":"Madrid" ,"B":"London" ,"C":"[dead]"})

    def test_dafterM_2(self):
        locations={"A":"Madrid","B":"London","C":"Paris"}
        armyValues ={"A":2,"B":1,"C":1}
        attackers = {"C":"B"}
        supporters = {"B":"A"}
        diplomacy_afterMath(locations,armyValues,attackers,supporters)
        self.assertEqual(locations,{"A":"Madrid" ,"B":"[dead]" ,"C":"[dead]"})

    def test_dafterM_3(self):
        locations={"A":"Madrid","B":"London","C":"Paris"}
        armyValues ={"A":1,"B":2,"C":1}
        attackers = {"B":"C"}
        supporters = {"A":"B"}
        diplomacy_afterMath(locations,armyValues,attackers,supporters)
        self.assertEqual(locations,{"A":"Madrid" ,"B":"Paris" ,"C":"[dead]"})


    def test_dsolve_1(self):
        r=StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w=StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    def test_dsolve_2(self):
        r=StringIO("A\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w=StringIO()
        self.assertEqual(diplomacy_solve(r,w),"Error invalid input")
    def test_dsolve_3(self):
        r=StringIO("A Madrid\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w=StringIO()
        self.assertEqual(diplomacy_solve(r,w),"Error invalid input")
    def test_dsolve_4(self):
        r=StringIO("A Madrid Support\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w=StringIO()
        self.assertEqual(diplomacy_solve(r,w),"Error invalid input")
    def test_dsolve_5(self):
        r=StringIO("A Madrid Move\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w=StringIO()
        self.assertEqual(diplomacy_solve(r,w),"Error invalid input")

    
# ----
# main
# ----

if __name__ == "__main__":
    main()
