from io import StringIO
from Diplomacy import diplomacy_solve
from unittest import main, TestCase

class TestDiplomacy (TestCase):

    # ----
    # eval
    # ----

    def test_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\n")


    def test_2(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_3(self):
        r = StringIO("A Austin Hold\nB Houston Move Austin\nC Dallas Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Dallas\n")

    def test_4(self):
        r = StringIO("A Austin Move Houston\nB Houston Move Austin\nC Dallas Move SanAntonio\nD SanAntonio Move Dallas")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Austin\nC SanAntonio\nD Dallas\n")

    def test_5(self):
        r = StringIO("A Austin Move Houston\nB Houston Hold\nC Dallas Support A\nD SanAntonio Move Dallas")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")



# main
# ----


if __name__ == "__main__": # pragma: no cover
    main()